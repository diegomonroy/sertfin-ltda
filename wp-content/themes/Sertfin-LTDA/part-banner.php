<!-- Begin Banner -->
	<section class="banner wow fadeIn" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
				<?php if ( is_page( array( 'historia' ) ) ) : dynamic_sidebar( 'banner_historia' ); endif; ?>
				<?php if ( is_page( array( 'mision' ) ) ) : dynamic_sidebar( 'banner_mision' ); endif; ?>
				<?php if ( is_page( array( 'vision' ) ) ) : dynamic_sidebar( 'banner_vision' ); endif; ?>
				<?php if ( is_page( array( 'avaluo-de-activos' ) ) ) : dynamic_sidebar( 'banner_avaluo_de_activos' ); endif; ?>
				<?php if ( is_page( array( 'inventario-de-activos' ) ) ) : dynamic_sidebar( 'banner_inventario_de_activos' ); endif; ?>
				<?php if ( is_page( array( 'inspeccion-de-garantias' ) ) ) : dynamic_sidebar( 'banner_inspeccion_de_garantias' ); endif; ?>
				<?php if ( is_page( array( 'contacto' ) ) ) : dynamic_sidebar( 'banner_contacto' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->
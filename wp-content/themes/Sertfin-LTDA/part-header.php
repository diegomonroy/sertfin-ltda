<!-- Begin Top -->
	<section class="top wow fadeIn" data-wow-delay="0.5s">
		<div class="row align-center align-middle">
			<div class="small-12 columns">
				<div class="show-for-small-only"><?php dynamic_sidebar( 'logo' ); ?></div>
				<?php get_template_part( 'part', 'menu' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->
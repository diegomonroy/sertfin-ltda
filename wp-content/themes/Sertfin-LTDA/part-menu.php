<!-- Begin Menu -->
	<div class="moduletable_to1">
		<div class="top-bar">
			<div class="top-bar-title">
				<span data-responsive-toggle="responsive-menu" data-hide-for="medium">
					<button class="menu-icon dark" type="button" data-toggle></button>
					<strong>Menú</strong>
				</span>
			</div>
			<div id="responsive-menu">
				<div class="top-bar-left">
					<?php
					wp_nav_menu(
						array(
							'menu_class' => 'dropdown menu align-center',
							'container' => false,
							'theme_location' => 'main-menu',
							'items_wrap' => '<ul class="%2$s" data-dropdown-menu>%3$s</ul>'
						)
					);
					?>
				</div>
			</div>
		</div>
	</div>
<!-- End Menu -->